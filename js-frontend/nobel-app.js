// Kelly Buchanan
// kbuchana
// Timothy Gallagher
// tgallag6

makeServerRequest("GET", -1, -1, '/laureates/');

search = document.getElementById("search-button");
// button event function
search.onmouseup = filterLaureates;

add = document.getElementById("add-button");
// button event function
add.onmouseup = addLaureate;

see = document.getElementById("see-button");
// button event function
see.onmouseup = seeLaureates;

reset = document.getElementById("reset-button");
// button event function
reset.onmouseup = resetLaureates;


var id = 0;

function getLaureates() {
  var allLaureates = [];

  allLaureates = document.getElementById("response-index").textContent;
  allLaureates = JSON.parse(allLaureates);
  allLaureates = allLaureates['laureates'];

  return allLaureates;

}

function seeLaureates() {
  var allLaureates = getLaureates();

  var str = '';

  if (allLaureates) {
    allLaureates.forEach(function(laur) {
      str += '<tr style="display:contents;">'
      str += '<td>' + laur['id'] + '</td>' +
        '<td>' + laur['name'] + '</td>' +
        '<td>' + laur['country'] + '</td>' +
        '<td>' + laur['year'] + '</td>' +
        '<td>' + laur['discipline'] + '</td>' +
        '<td><button style="padding-top:0px; max-height:20px;" type="submit" class="btn btn-primary" onmouseup="removeLaureate(' + laur['id'] + ')" id="remove-button">Remove </button></td>';
      str += '</tr>'
    });
  }

  document.getElementById('laureate-list').innerHTML = str;

}

function filterLaureates() {

  var allLaureates = getLaureates();
  var filteredLaureates = [];

  allLaureates.forEach((laureate) => {
    laureate['name'] = laureate['name'].toLowerCase();
    laureate['country'] = laureate['country'].toLowerCase();
    laureate['year'] = laureate['year'].toLowerCase();
    laureate['discipline'] = laureate['discipline'].toLowerCase();
  });
  var id = document.getElementById("laureate-id").value;

  if (id) {
    makeServerRequest("GET", id, -1, '/laureates/');
    laur = document.getElementById("response-key").textContent
    laur = JSON.parse(laur);

    var str = '';
    str += '<tr style="display:contents;">'
    str += '<td>' + laur['id'] + '</td>' +
      '<td>' + laur['name'] + '</td>' +
      '<td>' + laur['country'] + '</td>' +
      '<td>' + laur['year'] + '</td>' +
      '<td>' + laur['discipline'] + '</td>' +
      '<td><button style="padding-top:0px; max-height:20px;" type="submit" class="btn btn-primary" onmouseup="removeLaureate(' + laur['id'] + ')" id="remove-button">Remove </button></td>';
    str += '</tr>'

    document.getElementById('laureate-list').innerHTML = str;

  } else {
    var name = document.getElementById("laureate-name").value;
    var country = document.getElementById("laureate-country").value;
    var year = document.getElementById("laureate-year").value;
    var discipline = document.getElementById("laureate-discipline").value;

    if (name) {
      name = name.toLowerCase()
      allLaureates.forEach((laureate) => {
        if (laureate['name'].includes(name)) {
          filteredLaureates.push(laureate);
        }
      });
    }

    if (country) {
      country = country.toLowerCase()
      var tempArr = []
      if (filteredLaureates.length == 0) {
        filteredLaureates = allLaureates;
      }
      filteredLaureates.forEach((laureate) => {
        if (laureate['country'].includes(country)) {
          tempArr.push(laureate);
        }
      });
      filteredLaureates = tempArr;
    }


    if (year) {
      year = year.toLowerCase()
      var tempArr = []
      if (filteredLaureates.length == 0) {
        filteredLaureates = allLaureates;
      }
      filteredLaureates.forEach((laureate) => {
        if (laureate['year'].includes(year)) {
          tempArr.push(laureate);
        }
      });
      filteredLaureates = tempArr;
    }

    if (discipline) {
      discipline = discipline.toLowerCase()
      var tempArr = []
      if (filteredLaureates.length == 0) {
        filteredLaureates = allLaureates;
      }
      filteredLaureates.forEach((laureate) => {
        if (laureate['discipline'].includes(discipline)) {
          tempArr.push(laureate);
        }
      });
      filteredLaureates = tempArr;
    }
    var str = '';
    if (filteredLaureates.length == 0) {
      str = "No laureates match your search";
    } else {

      filteredLaureates.forEach(function(laur) {
        str += '<tr style="display:contents;">'
        str += '<td>' + laur['id'] + '</td>' +
          '<td>' + laur['name'] + '</td>' +
          '<td>' + laur['country'] + '</td>' +
          '<td>' + laur['year'] + '</td>' +
          '<td>' + laur['discipline'] + '</td>' +
          '<td><button style="padding-top:0px; max-height:20px;" type="submit" class="btn btn-primary" onmouseup="removeLaureate(' + laur['id'] + ')" id="remove-button">Remove </button></td>';
        str += '</tr>'
      });
      document.getElementById('laureate-list').innerHTML = str;
    }
  }
}

function addLaureate() {

  var name = document.getElementById("laureate-name").value;
  var country = document.getElementById("laureate-country").value;
  var year = document.getElementById("laureate-year").value;
  var discipline = document.getElementById("laureate-discipline").value;

  var newLaureate = {
    "id": "",
    "name": name,
    "country": country,
    "year": year,
    "discipline": discipline
  };

  makeServerRequest("POST", -1, JSON.stringify(newLaureate), "/laureates/");

    document.getElementById('note').innerHTML = "Laureate '" + name + "' was successfully added!";
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();



}

function removeLaureate(key) {
  makeServerRequest("DELETE", key, "{}", "/laureates/")
  response = document.getElementById("response-key").textContent
  if(!response) {
    document.getElementById('note').innerHTML = "Could not remove, try again.";
  } else {
    document.getElementById('note').innerHTML = "Laureate with id " + key + " successfully removed!";
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();
    seeLaureates();

  }
  makeServerRequest("GET", -1, -1, '/laureates/');
}

function makeServerRequest(req, key, body, data) {
  var URL = "http://student10.cse.nd.edu:51057";
  URL = URL + data;

  if (key != -1) {
    newUrl = URL + key;
  } else {
    key = null;
    newUrl = URL;
  }

  if (body == -1) {
    body = null;
  }
  // concat the values
  var values = [req, newUrl, body];

  /****** MAKE REQUEST TO SERVER *******/
  var response = null;
  // open http request
  var xhr = new XMLHttpRequest();
  xhr.open(values[0], values[1], true);

  xhr.onload = function(evnt) {
    if (xhr.readyState === 4) {
      if (key) {
        document.getElementById('response-key').innerHTML = xhr.responseText;
      } else if (req == "POST" || req == "PUT"){
        document.getElementById('response-add').innerHTML = xhr.responseText;
      } else {
        document.getElementById('response-index').innerHTML = xhr.responseText;
      }
    } else {
      //  error check
      console.error(xhr.statusText);
    }
  };

  xhr.onerror = function(evnt) {
    // error check
    console.error(xhr.statusText);
  }

  xhr.send(values[2])

}

function resetLaureates() {
  var reset = {};
  makeServerRequest("PUT", -1, JSON.stringify(reset), '/reset/');
  var response = document.getElementById('response-add').textContent;

  seeLaureates();
  seeLaureates();
  seeLaureates();
  seeLaureates();
  seeLaureates();
  seeLaureates();
  seeLaureates();

  location.reload();

}
