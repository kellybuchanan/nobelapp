import unittest
import requests
import json
from nobel_library import _laureates_database 

class TestOOAPI(unittest.TestCase):
    print("Testing OO API functions...")

    def test_load_laureates(self):
        # test function for loading the laureates from data file
        ndb = _laureates_database()
        ndb.load_laureates('laureates.dat')
        
        laureates = list(ndb.laureate_names.keys())
        self.assertEqual(955, len(laureates))
        self.assertEqual('26', laureates[24])

    def test_get_laureates(self):
        # test function for getting all laureates
        ndb = _laureates_database()
        ndb.load_laureates('laureates.dat')

        laureates = list(ndb.get_laureates())
        self.assertEqual(955, len(laureates))

    def test_get_laureate(self):
        # test function for getting one laureate
        ndb = _laureates_database()
        ndb.load_laureates('laureates.dat')

        laureate = ndb.get_laureate(26)
        self.assertEqual('Albert Einstein', laureate[0])
        self.assertEqual('1921', laureate[1])
        self.assertEqual('physics', laureate[2])
        self.assertEqual('Germany', laureate[3])

    def test_set_laureate(self):
        # test function for changing/setting a laureate
        ndb = _laureates_database()
        ndb.load_laureates('laureates.dat')

        laureate = list()
        laureate.append('Albie')
        laureate.append('2020')
        laureate.append('Candy')
        laureate.append('South Bend')
        

        ndb.set_laureate(26, laureate)
        laureate = ndb.get_laureate(26)

        self.assertEqual('Albie', laureate[0])
        self.assertEqual('2020', laureate[1])
        self.assertEqual('Candy', laureate[2])
        self.assertEqual('South Bend', laureate[3])

    def test_delete_laureate(self):
        # test function for delete a laureate
        ndb = _laureates_database()
        ndb.load_laureates('laureates.dat') 

        ndb.delete_laureate(26)

        old = ndb.get_laureate(26)

        self.assertEqual(old, None)

    
if __name__ == "__main__":
    unittest.main()
