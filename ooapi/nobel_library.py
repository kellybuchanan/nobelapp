#Kelly Buchanan
#kbuchana
#Timothy Gallagher
#tgallag6
import json

class _laureates_database:

       def __init__(self):
            self.laureate_names = dict()
            self.laureate_years = dict()
            self.laureate_discs = dict()
            self.laureate_conts = dict() 

       def load_laureates(self, laureate_file):
            # load all the laureates from the data file and sets dicts for categories
            f = open(laureate_file)
            data = json.load(f)

            for l in data['laureates']:
                lid = l['id']
                first = l['firstname']
                lname = first
                if 'surname' in l:
                    sur = l['surname']
                    lname = lname + ' ' + sur
                if 'bornCountry' in l:
                    lcont = l['bornCountry']
                else:
                    lcont = '--'
                pyear = l['prizes'][0]['year']
                pdisc = l['prizes'][0]['category']
                self.laureate_names[lid] = lname
                self.laureate_years[lid] = pyear
                self.laureate_discs[lid] = pdisc
                self.laureate_conts[lid] = lcont

            f.close()

       def get_laureates(self):
            # get all laureates by dictionary of id keys 
            return self.laureate_names.keys()

       def get_laureate(self, lid):
            # return a single laureate for specified id
            try:    
                lname = self.laureate_names[str(lid)]
                lcont = self.laureate_conts[str(lid)]
                pyear = self.laureate_years[str(lid)]
                pdisc = self.laureate_discs[str(lid)]
                laureate = list((lname, pyear, pdisc, lcont))
            except Exception as ex:
                laureate = None
            return laureate

       def set_laureate(self, lid, laureate):
            # set a laureate for the given id to match information passed in
            self.laureate_names[str(lid)] = laureate[0]
            self.laureate_years[str(lid)] = laureate[1]
            self.laureate_discs[str(lid)] = laureate[2]
            self.laureate_conts[str(lid)] = laureate[3]

       def delete_laureate(self, lid):
            # delete a laureate by clearing all categories for it
            del(self.laureate_names[str(lid)])
            del(self.laureate_discs[str(lid)])
            del(self.laureate_conts[str(lid)])
            del(self.laureate_years[str(lid)])

if __name__ == "__main__":
       ndb = _nobel_database()

       #### LAUREATES ########
       ndb.load_laureates('laureates.dat')

       laureate = ndb.get_laureate(26)
       print(laureate['name'])

       laureate['name'] = 'Albie Einst'
       ndb.set_laureate(26, laureate)

       laureate = ndb.get_laureate(26)
       print(laureate['name'])

       ####################


