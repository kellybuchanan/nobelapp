Kelly Buchanan
kbuchana

Timothy Gallagher
tgallag6

# Nobel Application

## Data source: 

    http://api.nobelprize.org/v1/laureate.json

## Milestone 1: Data, OO API and Rest API Server

### API Use:

    This API will be used to retrieve and manipulate data as an Administrator of an app that displays the Nobel Prize Laureates of the past. 
    It runs on http://student10.cse.nd.edu:51057. 

### JSON Specification:
<https://drive.google.com/file/d/1zfWQsC6_-ZdvwS0V_zXxBbTGUPtFGzTD/view?usp=sharing>

### To run tests:
   
    1. cd server/
    2. python3 server.py
    3. python3 test_ws.py (in different terminal window)
    4. cd ../ooapi
    5. python3 test_api.py


## Milestone 2: JS front end

### Application Website:

<http://kellybuchanan.gitlab.io/nobelapp/js-frontend/>

### To run application:

    1. ssh into student10.cse.nd.edu
    2. cd server/
    3. python3 server.py
    4. go to website for the application 

### User Interaction

    1. Search for Laureates by fields
    - Enter any number of fields you want the laureate to match (name, country, year, discipline)
    - Hit 'Search'

    2. Search for Laureate by ID
    - Enter ID of the laureate you want to see
    - Hit 'Search'
    - If it does not load after hitting Search, wait a second then hit it again
    (since JavaScript is asynchronous, sometimes the result of the HTTP request does not return by the time the program attempts to add it to the html file)

    3. See all laureates
    - Hit 'See All'

    4. Remove Laureates
    - Whenever a laureate or laureates are displayed, you will see a Remove button
    - Hit 'Remove' next to whichever you would like to remove
    - See changes by searching for them or by seeing all

    5. Add Laureates
    - Enter information for laureate you would like to add in field locations
    - Hit 'Add'
    - See results by searching for them or by seeing all

    6. Clear all changes made to list
    - Hit 'Clear all Changes'
    - See results by searching for them or by seeing all

## Milestone 3: Final videos and deliverables

### Videos:

    UI Demo:
        <https://drive.google.com/file/d/17m7xVKQNGDmT-JKTKcbmA-66ydlZRCPm/view?usp=sharing>
    
    Code Walkthrough:
        <https://drive.google.com/file/d/1Uoap7PQgA2nf1nU58enqqriyEMQNhR9u/view?usp=sharing>

### Scale and Complexity:

    - Very large data set with 1000 entries, each of which nested with at least three JSON objects. This allows for very unique and complex parsing of the data and allows the user to get precise information on nobel laureates. 
    - Works with two server site URLS, one with the laureate database that can be modified and one to reset that database from all changes. 
    - If we were to continue with this project, another direction we could take would be scraping wikipedia for the laureates or adding an advanced search with more key options. 

### Presentation:

<https://docs.google.com/presentation/d/1d_VsfmaOCzg_PRgsEiwj9vgByQRmyB28HKBB_F3hFgw/edit?usp=sharing>

## Files:
 
`ooapi/`

- `laureates.dat` - data file holding json objects for nobel prize laureates loaded from data url
- `nobel_library.py` - python library containing main class _laureates_database
- `test_api.py` - test file for OO API functions

`server/`
 
- `laureatesController.py` - python file holding the handler functions for server requests to /laureates/
- `resetController.py` - python file holding the handler functions for server requests to /reset/
- `server.py` - REST API server file with dispatch and connection handlers
- `test_ws.py` - test for all endpoints for webservice

`js-frontend`
- `index.html` - HTML file for user interface and frontend display
- `nobel-app.js` - JavaScript File for functionality of user interaction in the webservice



