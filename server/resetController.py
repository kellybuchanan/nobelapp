# Kelly Buchanan
# kbuchana
# Timothy Gallagher
# tgallag6

import cherrypy
import re, json
import sys
sys.path.append('../ooapi/')
from nobel_library import _laureates_database

class ResetController(object):

    def __init__(self, ndb=None):
        if ndb is None:
            self.ndb = _laureates_database()
        else:
            self.ndb = ndb


    def PUT_INDEX(self):
        # handler for PUT request to reset all laureate to original
        output = {'result':'success'}

        data = json.loads(cherrypy.request.body.read().decode())

        self.ndb.__init__()
        self.ndb.load_laureates('../ooapi/laureates.dat')

        return json.dumps(output)

    def PUT_KEY(self, lid):
        # handler for PUT request to reset one laureate to original
        output = {'result':'success'}
        lid = int(lid)

        try:
            data = json.loads(cherrypy.request.body.read().decode())

            ndbtmp = _laureates_database()
            ndbtmp.load_laureates('../ooapi/laureates.dat')

            laureate = ndbtmp.get_laureate(lid)
            
            self.ndb.set_laureate(lid, laureate) 


        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
