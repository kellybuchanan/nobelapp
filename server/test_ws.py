import unittest
import requests
import json

class TestWS(unittest.TestCase):

    SITE_URL = 'http://student10.cse.nd.edu:51057' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    LAUREATES_URL = SITE_URL + '/laureates/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        # function to reset data using reset endpoint
        l = {}
        r = requests.put(self.RESET_URL, json.dumps(l))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False



        ############# LAUREATE ENDPOINT: INDEX ################


    def test_laureates_index_get(self):
        # test function for get index (get all laureates)
        self.reset_data()
        r = requests.get(self.LAUREATES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testlaur = {}
        laureates = resp['laureates']

        for laur in laureates:
            if laur['id'] == str(26):
                testlaur = laur

        self.assertEqual(testlaur['name'], 'Albert Einstein')
        self.assertEqual(testlaur['year'], '1921')

    def test_laureates_index_post(self):
        # test function for post index (add laureate)
        self.reset_data()

        l = {}
        l['name'] = 'John Doe'
        l['discipline'] = 'Physics'
        l['year'] = '2020'
        l['country'] = 'USA'

        r = requests.post(self.LAUREATES_URL, data = json.dumps(l))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['id'], 997)

        r = requests.get(self.LAUREATES_URL + str(resp['id']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['name'], l['name'])
        self.assertEqual(resp['year'], l['year'])
    


        ############# LAUREATE ENDPOINT: KEY ################


    def test_laureates_get_key(self):
        # test funciton for get key (get one laureate)
        self.reset_data()
        lid = 26
        r = requests.get(self.LAUREATES_URL + str(lid))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))

        self.assertEqual(resp['name'], 'Albert Einstein')
        self.assertEqual(resp['year'], '1921')

    def test_laureates_put_key(self):
        # test function for put key (alter a laureate)
        self.reset_data()
        lid = 26
        r = requests.get(self.LAUREATES_URL + str(lid))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['name'], 'Albert Einstein')
        self.assertEqual(resp['year'], '1921')
        
        l = {}
        l['name'] = 'Albie Einst'
        l['year'] = '2021'
        l['discipline'] = resp['discipline']
        l['country'] = resp['country']
        r = requests.put(self.LAUREATES_URL + str(lid), data = json.dumps(l))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.LAUREATES_URL + str(lid))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['name'], l['name'])
        self.assertEqual(resp['year'], l['year'])

    def test_laureates_delete_key(self):
        # test function for delete key (delete a laureate)
        self.reset_data()
        lid = 26

        l = {}
        r = requests.delete(self.LAUREATES_URL + str(lid), data = json.dumps(l))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.LAUREATES_URL + str(lid))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')



        ############# RESET ENDPOINT ################


    def test_put_reset_index(self):
        # test function for reset index (reset all laureates)
        l = {}
        r = requests.put(self.RESET_URL, json.dumps(l))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        r = requests.get(self.LAUREATES_URL)
        resp = json.loads(r.content.decode('utf-8'))
        laureates = resp['laureates']
        self.assertEqual(laureates[24]['name'], 'Albert Einstein')
        self.assertEqual(laureates[24]['year'], '1921')


    def test_put_reset_key(self):
        # test function for reset index (reset one laureates)
        l = {}
        r = requests.put(self.RESET_URL, json.dumps(l))
        lid = 26

        l['name'] = 'Albie Einst'
        l['year'] = '2010'
        l['discipline'] = 'Candy'
        l['country'] = 'USA'

        r = requests.put(self.LAUREATES_URL + str(lid), data = json.dumps(l))

        l = {}
        r = requests.put(self.RESET_URL + str(lid), data = json.dumps(l))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.LAUREATES_URL+ str(lid), data = json.dumps(l))
        resp1 = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp1['name'], 'Albert Einstein')    
        self.assertEqual(resp1['year'], '1921')


if __name__ == "__main__":
    unittest.main()

