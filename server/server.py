# Kelly Buchanan
# kbuchana
# Timothy Gallagher
# tgallag6

import cherrypy
import sys
from laureatesController import LaureatesController
from resetController import ResetController
sys.path.append('../ooapi')
from nobel_library import _laureates_database

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"


def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()


    ndb = _laureates_database()

    laureatesController = LaureatesController(ndb=ndb) 
    resetController     = ResetController(ndb=ndb)
    
    # connect dispatchers for all HTTP requests used in this REST API
    dispatcher.connect('laureate_get', '/laureates/:lid', controller=laureatesController, action = 'GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('laureate_put', '/laureates/:lid', controller=laureatesController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('laureate_delete', '/laureates/:lid', controller=laureatesController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('laureate_index_get', '/laureates/', controller=laureatesController, action = 'GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('laureate_index_post', '/laureates/', controller=laureatesController, action = 'POST_INDEX', conditions=dict(method=['POST']))


    dispatcher.connect('reset_put', '/reset/:lid', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    # CORS related options connections
    dispatcher.connect('laureate_key_options', '/laureates/:lid', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('laureate_options', '/laureates/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_key_options', '/reset/:lid', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    

    conf = {
	'global': {
            'server.thread_pool': 5, # optional argument
	    'server.socket_host': 'student10.cse.nd.edu', # 
	    'server.socket_port': 51057, #change port number to your assigned
	    },
	'/': {
	    'request.dispatch': dispatcher,
            'tools.CORS.on':True,
	    }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# end of start_service


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()


