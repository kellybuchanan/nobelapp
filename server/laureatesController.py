# Kelly Buchanan
# kbuchana
# Timothy Gallagher
# tgallag6

import cherrypy
import re, json
import sys
sys.path.append('../ooapi/')
from nobel_library import _laureates_database

class LaureatesController(object):

        def __init__(self, ndb=None):
                if ndb is None:
                    self.ndb = _laureates_database()
                else:
                    self.ndb = ndb

                self.ndb.load_laureates('../ooapi/laureates.dat')

        def GET_KEY(self, lid):
                # handler for GET request for one laureate
                output = {'result':'success'}
                lid = int(lid)
                try:
                    laureate = self.ndb.get_laureate(lid)
                    if laureate is not None:
                        output['id'] = lid
                        output['name'] = laureate[0]
                        output['year'] = laureate[1]
                        output['discipline'] = laureate[2]
                        output['country'] = laureate[3]
                    else:
                        output['result'] = 'error'
                        output['message'] = 'laureate not found'
                except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex)

                return json.dumps(output)

        def GET_INDEX(self):
                # handler for GET request for all laureates
                output = {'result':'success'}
                output['laureates'] = []

                try:
                    for lid in self.ndb.get_laureates():
                        laureate = self.ndb.get_laureate(lid)
                        dlaureate = {'id':lid, 'name':laureate[0],'discipline':laureate[2], 'year':laureate[1], 'country':laureate[3]}
                        output['laureates'].append(dlaureate)
                except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex)

                return json.dumps(output)

        def DELETE_KEY(self, lid):
                # handler for DELETE request for a single laureate
                output = {'result' : 'success'}
                lid = int(lid)

                try:
                    self.ndb.delete_laureate(lid)
                except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex)

                return json.dumps(output)

        def PUT_KEY(self, lid):
                # handler for a PUT request for changing one laureate
                output = {'result':'success'}
                lid = int(lid)

                data = json.loads(cherrypy.request.body.read().decode('utf-8'))

                laureate = list()
                laureate.append(data['name'])
                laureate.append(data['year'])
                laureate.append(data['discipline'])
                laureate.append(data['country'])

                self.ndb.set_laureate(lid, laureate)

                return json.dumps(output)

        def POST_INDEX(self):
                # handler for POST request to add laureate to list
                output = {'result': 'success'}
                data = json.loads(cherrypy.request.body.read())
                laureate = list()

                laureates = list(self.ndb.get_laureates())
                maxx = int(laureates[len(laureates)-1]) + 1
            
                output['id'] = maxx

                try:
                    laureate.append(data['name'])
                    laureate.append(data['year'])
                    laureate.append(data['discipline'])
                    laureate.append(data['country'])
                    self.ndb.set_laureate(maxx, laureate)
                except Exception as ex:
                    output['result']    = 'error'
                    output['message']   = str(ex)

                return json.dumps(output)
